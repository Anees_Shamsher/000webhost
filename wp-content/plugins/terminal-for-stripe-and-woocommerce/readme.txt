=== Terminal for Stripe and WooCommerce ===
Contributors: arcanedevops
Donate link: https://www.arcanestrategies.com/
Plugin Name: Terminal for Stripe and WooCommerce
Plugin URI: https://www.arcanestrategies.com/products/#product-stripe-terminal-for-woocommerce
Author URI: https://www.arcanestrategies.com/
Author: Arcane Strategies
Tags: stripe, woocommerce, stripe terminal, verifone, point of sale, in person payments, pos, p400, card reader, ecommerce, e-commerce, store, sales, sell, shop, cart, checkout, payments, storefront, woo
Version: 1.2.0
Requires at least: 4.4
Tested up to: 5.4
Requires PHP: 7.0
Requires WooCommerce: 2.6
WooCommerce tested up to: 4.0.1
Requires WooCommerce Stripe Gateway: 4.2
Stable: 1.2.0
License: GPLv3
License URI: https://www.gnu.org/licenses/gpl-3.0.html

Terminal for Stripe and WooCommerce connects in-person transactions to your website. Built on WordPress, WooCommerce, and WooCommerce Stripe Gateway, it permits a Verifone P400 POS to integrate directly to your website.

== Description ==

Terminal for Stripe and WooCommerce connects in-person transactions to your website. Built on WordPress, WooCommerce, and WooCommerce Stripe Gateway, it permits a Verifone P400 POS to integrate directly to your website.

To activate the Free Terminal for Stripe and WooCommerce plugin on an existing WooCommerce in minutes, follow our [installation guide](https://wordpress.org/plugins/terminal-for-stripe-and-woocommerce/#installation).

Terminal for Stripe and WooCommerce is developed and supported by Arcane Strategies, the creators of Witmap.io and Skipwish.com.

[youtube https://www.youtube.com/watch?v=OMlidhenDBo]
[youtube https://www.youtube.com/watch?v=3iQNV_P2kVk]

Click on the following link to find instructions on how to install your [Stripe Terminal PRO](https://store.arcanestrategies.com/products/stripe-terminal-for-woocommerce)

== Features ==

* In-Person Payments Connected to WooCommerce and Stripe
* 1 Location with Multiple Verifone Readers
* All Current Stripe and Verifone P400 Features
* Supports Apple Pay, Google Pay, and Samsung Pay
* Pre-Dip Cards
* Supports BBPOS Chipper through Arcane's mobile appliation

= Additional Features =

For a more robust solution, check out the features of our Pro Version for [Stripe Terminal](https://www.arcanestrategies.com/portfolio/stripe-terminal-for-woocommerce/). To upgrade, click on the following link to purchase our PRO Version for [Stripe Terminal](https://store.arcanestrategies.com/products/stripe-terminal-for-woocommerce)
Support for Pro Version is available at [www.ArcaneStrategies.com](https://www.arcaneStrategies.com). Terminal for Stripe and WooCommerce is developed and supported by [Arcane Strategies](https://www.arcaneStrategies.com), the creators of Witmap.io and [Skipwish.com](https://www.skipwish.com).

Please subscribe to our mailchimp list to receive updates on new features [https://www.arcanestrategies.com/subscribe/](https://www.arcanestrategies.com/subscribe/):

= Minimum Requirements =

* PHP 7.0 or greater is recommended
* MySQL 5.6 or greater is recommended
* In order for the terminal option to be available, you must be logged in as an authorized WordPress administrator or Store Manager (otherwise all your site visitors would see it).
* All orders are associated with cashier, so this will not support account-specific orders (ie. subscriptions)

Visit the [WooCommerce server requirements documentation](https://docs.woocommerce.com/document/server-requirements/?utm_source=wp%20org%20repo%20listing&utm_content=3.6) for a detailed list of server requirements.

= Usage =

* You must be logged in as a WooCommerce store manager (ie. role "Administrator" or "Store Manager") in order to use the Terminal on the frontend.
* While logged in, navigate your customer through the checkout experience. You'll be given the option to select the terminal reader icon to process accordingly.
* After selecting the terminal option, press the button for the reader you set up during installation.
* Credit card prompts will expire after 30 seconds but may be manually canceled by hitting the "Esc" key on your keyboard.

== Installation ==

1. Download the plugin, unzip it, and upload it to `/wp-content/plugins/` directory via FTP or install the plugin through the WordPress plugins screen directly (see: https://wordpress.org/support/article/managing-plugins/#manual-plugin-installation).
2. Activate the plugin through the 'Plugins' screen in WordPress.
3. Click the plugin's "settings" link and follow the below instructions.

* Make sure your terminal is turned on and connected to the internet.
* In a separate tab, open your WooCommerce Payment settings and verify that the Stripe Gateway has "Payment Butotns" enabled.
* On the plugins page, select "Settings".  Here is where you can configure your settings.  For BBPOS Chipper use, be sure your WordPress site's permalinks are set to enable pretty URLs (post name).
* To set up a reader, type the registration code into your terminal device: 0-7-1-3-9
* Now click the "Add Reader" button and type into the prompt the registration code seen on your terminal.  This is usually a 3 word string.
* Enter a recognizable name in the prompt and save.  This will be how it appears on checkout.

This plugin may encounter issues with the "Merge + Minify + Refresh" plugin and other compile and minify production asset plugins.  Performance may be impacted by WP Live Chat as that codebase contains uncaught errors.  If problems are encountered, simply deactivate your minification plugin, reload the page, then reactivate the minification plugin to rebuild the cache with the new files.

[youtube https://www.youtube.com/watch?v=OMlidhenDBo]

== Frequently Asked Questions ==

= I keep getting a generic API error, what gives? =
This could be a number of things but more often than not it is because of the following:
(1) The plugin is not enabled (see "activate" and settings "enable" checkbox, (2) The Stripe Gateway payment method within WooCommerce is disabled OR API keys are incorrect, (3) The Stripe Gateway's "Payment Buttons" option is disabled.
= What other plugins are required?  =
WooCommerce and WooCommerce Stripe Gateway.  Supported versioning can be found in the README file.
= How can I get help with integration?  =
Requests may be submitted to support at arcanestrategies.com.  Support requests start at $225.
= How do I add a reader? =
To add a reader, first make sure that the P400 reader is turned on and connected to the same network as your computer.  Then visit the plugin's "settings" page and click "Add Reader".  Then go to your device, type in the registration code (usually 0-7-1-3-9) and it will return a key (usually a 3 word string).  Now enter that string into the prompt on your WordPress site, give it a name, and you're done!.  For more details please check out the instruction section which contains all configuration details.
= I can't see the reader on checkout, any idea why? =
The reader will only show on checkout if (a) the p400 is powered on, (b) the terminal is registered, (c) a user with woocommerce store management permissions is logged in, (d) the cart contains only simple products.  This version is intended only to provide a retail point-of-sale experience in which an authorized representative is logged in to accept credit cards.  This is both for tracking who has accepted charges as well as to prevent the public from seeing the terminal icon while remotely viewing the site.  Because of this, purchases which require customer information (such as recurring billing info or email addresses) cannot be performed as the current session contains the administrator's information.  If you need to work around that, visit our website and purchase "Pro Services" (https://www.arcanestrategies.com/products/stripe-terminal-for-woocommerce)
= Will your plugin work outside the US? =
Our plugin will support any location that Stripe supports, there's no business logic implemented which would prevent support for our plugin in a region where Stripe Terminal is available.  As of date of this writing (January 2020), Stripe Terminal is only supported in North America (US and Canada).  Although Stripe does allow transactions overseas for cards typically used in European countries like Maestro, PIN transactions for those unsupported regions are not available.  Additionally, the readers themselves are not available for purchase outside the US.  As soon as Stripe announces support overseas, anyone who installs either of our plugins will receive notification.
= Does this plugin work with the BBPOS Chipper? =
The BBPOS Chipper is not a web-based device, it is BlueTooth connected via the iOS/Android mobile SDK's (for native applications), so it will not work on a website.  You may read more information on our blog at [https://www.arcanestrategies.com/blog/stripe-credit-card-readers/](https://www.arcanestrategies.com/blog/stripe-credit-card-readers/).
= Where can I buy the reader? =
Our support team will only support the readers purchased directly from Stripe.com or [https://store.arcanestrategies.com/](https://store.arcanestrategies.com/).  Many sources sell only the device without the cables (Amazon, eBay, CDW) and the cables are suprisingly expensive, it's not worth the gamble (trust us, we've tried).  Also, the P400 by default does not come equipped with the Stripe Terminal software pre-installed unless it's purchased from Stripe.com which means you won't be able to use the device with Stripe out-of-the-box.
= Why doesn't the cancel button work? =
To cancel an order, hit the "esc" key on your keyboard or wait for the timeout to expire.  The Verifone P400's action buttons (red, yellow, green) are intended to support settings, they do not cancel or edit payments. As soon as Stripe and Verifone make these programmable, they'll be implemented into this plugin.
= Are PIN transactions supported? =
Unfortunately Stripe Terminal does not support PIN transactions but they do support debit via signature.  We spoke with Stripe and they indicated they have no plan in making PIN transactions possible as they believe that signature debit transactions is sufficient.
= Why am I getting an error connecting to my reader?  I registered it without a problem! =
Whe you register your device, Stripe Terminal will assign your P400 to a domain on your private network.  This makes it reachable by the API.  Some Domain Name Servers do not play nicely with that setup, so they may block that traffic.  We suggest that you try switching your network's DNS provider to CloudFlare (1.1.1.1) or Google (8.8.8.8).  Although Stripe states that Google should work, we've had customers indicate that it does not, so try both.
= Why isn't the customer's information shown on the invoice and email receipt? =
Please review the documentation above.  Customer account information is not supported in this version, you must purchase PRO Services.  Out-of-the-box, Stripe Terminal does not gather PII data and WooCommerce will bill to the account which is logged in (the admin performing the checkout).  Our PRO Services add-on allows for customer account support to perform purchases for things like subscriptions.
= How can I enabled the BBPOS Chipper? =
Within the plugin's settings, (1) enter your Stripe Account ID, (2) within your wordpress permalink settings, make sure you have pretty URLs enabled (post anme), (3) click on the "Stripe Connect" button to complete account connection, (4) download our plugin (see link in Settings) and follow in-app instructions. 

== Screenshots ==

1.  This is how the terminal appears in your checkout

== Changelog ==

= 1.2.0 = 2020-06-20
* Announcement for Stripe Connect

= 1.1.4 = 2020-09-29
* Support for BBPOS Chipper, ArcanePOS, Customer Payment Page

= 1.1.3 = 2020-08-03
* Support for Canadian currencies.

= 1.1.2 = 2020-05-19
* Critical Upgrade.  Prevents simultaneous load of FREE and LITE classes which have conflicting classes.  Without this, enabling both will cause 500 erros.

= 1.1.1 = 2020-04-20
* Adds fingerprint storage.  This is a hugely important update as it makes for easier customer-order synchronization on PRO services.

= 1.1.0 = 2020-03-24
* Includes promotions and discounts for active users

= 1.0.9 = 2020-02-19
* Allows payment intents for invalid reader displays

= 1.0.7 = 2020-02-06
* Added notification opt-in for plugin users to be notified of updates

= 1.0.6 = 2020-01-08
* Created stripe terminal child class to comply with Stripe's requirements for registered plugins

= 1.0.5 = 2019-12-20
* Fixes a retry setting conflict with PRO Services

= 1.0.4 = 2019-12-19
* Include missed error handling for the Stripe JS SDK
* Resolves conflicts with Pro Services.

= 1.0.3 - 2019-12-12 =
* Attempt to retry to process an order when an error occurs

= 1.0.2 - 2019-12-10 =
* Support for shipping products
* Delete readers action updated to remove on Stripe
* Support for php 7.0.  To get support for php5, please contact support@arcanestrategies.com
[See changelog for all versions](https://bitbucket.org/arcanestrategies/woocommerce_stripe_terminal/src/master/CHANGELOG.txt).

= 1.0.1 - 2019-11-14 =
* Changes to WP Listing and Settings UI
[See changelog for all versions](https://bitbucket.org/arcanestrategies/woocommerce_stripe_terminal/src/master/CHANGELOG.txt).

= 1.0.0 - 2019-10-15 =
* Initial release
[See changelog for all versions](https://bitbucket.org/arcanestrategies/woocommerce_stripe_terminal/src/master/CHANGELOG.txt).

== Upgrade Notice ==

= 1.2.0 =
1.2.0 Announcement for Stripe Connect

= 1.1.4 =
1.1.4 Support for BBPOS Chipper, ArcanePOS, Customer Payment Page

= 1.1.3 =
1.1.3 Support for Canadian currencies.

= 1.1.2 =
1.1.2 Critical Upgrade.  Prevents simultaneous load of FREE and LITE classes which have conflicting classes.  Without this, enabling both will cause 500 erros.

= 1.1.1 =
1.1.1 Adds fingerprint storage.  This is a hugely important update as it makes for easier customer-order synchronization on PRO services

= 1.1.0 =
1.1.0 Includes promotions and discounts for active users

= 1.0.9 =
1.0.9 Allows payment intents for invalid reader displays

= 1.0.7 =
1.0.7 Added notification opt-in for plugin users to be notified of updates

= 1.0.6 =
1.0.6 complies with Stripe's partner requirements as a registered plugin.

= 1.0.5 =
1.0.5 fixes a retry setting conflict with PRO Services.

= 1.0.4 =
1.0.4 addresses missed error handling for the Stripe JS SDK and resolves conflicts with Pro Services.

= 1.0.3 =
1.0.3 adds the functionality to attempt to reprocess a payment if a functional error occurs.
[review update best practices](https://docs.woocommerce.com/document/how-to-update-your-site) before upgrading.

= 1.0.2 =
1.0.2 addresses a bug related to shipping costs in a cart and deleting readers on stripe. Also extends support down to php 7.0
[review update best practices](https://docs.woocommerce.com/document/how-to-update-your-site) before upgrading.

= 1.0.1 =
1.0.1 contains changes to the WP listing and Settings UI
[review update best practices](https://docs.woocommerce.com/document/how-to-update-your-site) before upgrading.

= 1.0 =
1.0 is the initial release
[review update best practices](https://docs.woocommerce.com/document/how-to-update-your-site) before upgrading.