<?php
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

class WC_TFSW_API{
	private static $APP_VERSION = '1.0.0';
	private static $DEFAULT_TIMEZONE = 'UTC';
	private static $HASH_ALGO = 'sha512';
	private static $KEY_SIZE = 64;
	private static $API_KEY_TTL = 86400; // 24 hours
	private static $instance;

	private $api_namespace = 'tfsw/v1';
	private $api_routes;

	private $gateway_instance;

	/**
	 * Private clone method to prevent cloning of the instance of the
	 * *Singleton* instance.
	 *
	 * @return void
	 */
	private function __clone() {}

	/**
	 * Private unserialize method to prevent unserializing of the *Singleton*
	 * instance.
	 *
	 * @return void
	 */
	private function __wakeup() {}

	/**
	 * Protected constructor to prevent creating a new instance of the
	 * *Singleton* via the `new` operator from outside of this class.
	 */
	private function __construct() {
		$this->gateway_instance = new Tfsw_Gateway_Stripe_Terminal();
		$this->api_routes = array(
			array(
				'route' => 'auth',
				'endpoints' => array(
					'methods' => 'POST',
					'callback' => array( $this, 'auth' ),
					'permission_callback' => '__return_true'
				)
			),
			array(
				'route' => 'account',
				'endpoints' => array(
					'methods' => 'GET',
					'callback' => array( $this, 'account' ),
					'permission_callback' => '__return_true'
				)
			),
			array(
				'route' => 'version',
				'endpoints' => array(
					'methods' => 'GET',
					'callback' => array( $this, 'version' ),
					'permission_callback' => '__return_true'
				)
			),
			array(
				'route' => 'products',
				'endpoints' => array(
					'methods' => 'GET',
					'callback' => array( $this, 'products' ),
					'permission_callback' => '__return_true'
				)
			),
			array(
				'route' => 'products/(?P<id>[\d]+)',
				'endpoints' => array(
					'methods' => 'GET',
					'callback' => array( $this, 'product' ),
					'permission_callback' => '__return_true'
				)
			),
			array(
				'route' => 'products/(?P<id>[\d]+)/variants',
				'endpoints' => array(
					'methods' => 'GET',
					'callback' => array( $this, 'product_variants' ),
					'permission_callback' => '__return_true'
				)
			),
			array(
				'route' => 'order',
				'endpoints' => array(
					'methods' => 'POST',
					'callback' => array( $this, 'create_order' ),
					'permission_callback' => '__return_true'
				)
			),
			array(
				'route' => 'order/(?P<id>[\d]+)',
				'endpoints' => array(
					array(
						'methods' => 'GET',
						'callback' => array( $this, 'get_order' ),
						'permission_callback' => '__return_true'
					),
					array(
						'methods' => 'PUT',
						'callback' => array( $this, 'update_order' ),
						'permission_callback' => '__return_true'
					),
					array(
						'methods' => 'DELETE',
						'callback' => array( $this, 'delete_order' ),
						'permission_callback' => '__return_true'
					)
				)
			),
			array(
				'route' => 'order/(?P<id>[\d]+)/shipping/methods',
				'endpoints' => array(
					'methods' => 'GET',
					'callback' => array( $this, 'shipping_methods' ),
					'permission_callback' => '__return_true'
				)
			)
		);

		add_action( 'rest_api_init', array( $this, 'register_routes' ) );
		$this->add_requirements();
	}

	/**
	 * Returns the *Singleton* instance of this class.
	 *
	 * @return Singleton The *Singleton* instance.
	 */
	public static function get_instance() {
		if ( null === self::$instance ) {
			self::$instance = new self();
		}
		return self::$instance;
	}

	/**
	 * Registers the api endpoints to wp-json.
	 *
	 * @since 1.1.4
	 * @version 1.1.4
	 */
	public function register_routes() {
		foreach($this->api_routes as $route){
			register_rest_route( $this->api_namespace, $route['route'], $route['endpoints'] );
		}
	}

	/**
	 * Require necessary classes.
	 *
	 * @since 1.1.4
	 * @version 1.1.4
	 */
	private function add_requirements(){
		require_once dirname(dirname(dirname( __FILE__ ))) . '/woocommerce/includes/legacy/api/v3/class-wc-api-exception.php';
		require_once dirname(dirname(dirname( __FILE__ ))) . '/woocommerce/includes/legacy/api/v3/interface-wc-api-handler.php';
		require_once dirname(dirname(dirname( __FILE__ ))) . '/woocommerce/includes/legacy/api/v3/class-wc-api-json-handler.php';
		require_once dirname(dirname(dirname( __FILE__ ))) . '/woocommerce/includes/legacy/api/v3/class-wc-api-server.php';
		require_once dirname(dirname(dirname( __FILE__ ))) . '/woocommerce/includes/legacy/api/v3/class-wc-api-resource.php';
		require_once dirname(dirname(dirname( __FILE__ ))) . '/woocommerce/includes/legacy/api/v3/class-wc-api-orders.php';
	}

	/**
	 * Generates a new API Key and returns it along with the hash for storage.
	 *
	 * @since 1.1.4
	 * @version 1.1.4
	 */
	private function generate_api_key(){
		$key = '';

        while(($len = strlen($key)) < self::$KEY_SIZE){
            $size = self::$KEY_SIZE - $len;

            $bytes = random_bytes($size);

            $key .= substr( str_replace( ['/', '+', '='], '', base64_encode( $bytes ) ), 0, $size );
		}

		$hash = hash( self::$HASH_ALGO, $key );
		return array( 'key' => $key, 'hash' => $hash );
	}

	/**
	 * Validates that the provided user id and stfw-token pair are valid.
	 *
	 * @since 1.1.4
	 * @version 1.1.4
	 */
	private function validate_token(WP_REST_Request $request){
		$invalid = new WP_Error( 'invalid_token', __( 'Your API token was invalid.', 'terminal-for-stripe-and-woocommerce' ), array( 'status' => 401 ) );

		$user_id = $request->get_header('User');
		$key = $request->get_header('Stfw-Token');
		if(empty( $user_id ) || empty( $key )){
			return new WP_Error( 'missing_token', __( 'You must provide a valid API token and User ID to access this endpoint.', 'terminal-for-stripe-and-woocommerce' ), array( 'status' => 401 ) );
		}

		$user_id = intval( $user_id );
		$key = hash( self::$HASH_ALGO, $key );
		$st_settings = get_option( 'woocommerce_stripe_terminal_settings' );
		if(!isset( $st_settings['api_keys'] ) || !is_array( $st_settings['api_keys'] )){
			$st_settings['api_keys'] = array();
			update_option( 'woocommerce_stripe_terminal_settings', $st_settings );
			return $invalid;
		}

		if(empty( $st_settings['api_keys'][$user_id] )){
			return $invalid;
		}

		if(( new DateTime( $st_settings['api_keys'][$user_id]['expires'] ) ) <= ( new DateTime( 'now', new DateTimeZone( self::$DEFAULT_TIMEZONE ) ) )){
			unset( $st_settings['api_keys'][$user_id] );
			update_option( 'woocommerce_stripe_terminal_settings', $st_settings );
			return new WP_Error( 'expired_token', __( 'Your API token has expired.', 'terminal-for-stripe-and-woocommerce' ), array( 'status' => 401 ) );
		}

		if(!hash_equals( $st_settings['api_keys'][$user_id]['key'], $key )){
			return $invalid;
		}

		wp_set_current_user( $user_id );
		return true;
	}

	/**
	 * Authenticates a user with the provided email/password combination and generates an API token if successful.
	 *
	 * @since 1.1.4
	 * @version 1.1.4
	 */
	public function auth(WP_REST_Request $request){
		if(!$request->has_param( 'email' ) || !$request->has_param( 'password' )){
			return new WP_Error( 'missing_params', __( 'Missing required parameters.', 'terminal-for-stripe-and-woocommerce' ), array( 'status' => 400 ) );
		}

		$user = wp_authenticate( sanitize_text_field( $request->get_param( 'email' ) ), sanitize_text_field( $request->get_param( 'password' ) ) );
		if(is_wp_error( $user )){
			return new WP_Error( $user->get_error_code(), strip_tags( $user->get_error_message() ), array( 'status' => 401 ) );
		}

		if(!$user->has_cap( 'manage_woocommerce' )){
			return new WP_Error( 'forbidden', 'You are not authorized to use this API.', array( 'status' => 403 ) );
		}

		$api_key = $this->generate_api_key();
		$st_settings = get_option( 'woocommerce_stripe_terminal_settings' );
		if(!isset( $st_settings['api_keys'] ) || !is_array( $st_settings['api_keys'] )){
			$st_settings['api_keys'] = array();
		}

		$expires = (new DateTime( 'now', new DateTimeZone( self::$DEFAULT_TIMEZONE ) ))->add( new DateInterval( 'PT' . self::$API_KEY_TTL . 'S' ) )->format( 'c' );
		$st_settings['api_keys'][$user->ID] = array( 'key' => $api_key['hash'], 'expires' => $expires );
		update_option( 'woocommerce_stripe_terminal_settings', $st_settings );
		
		return new WP_REST_Response( array(
			'id' => $user->ID,
			'stfw_token' => $api_key['key'],
			'expires' => $expires
		) );
	}

	/**
	 * Returns the stripe connect account id.
	 *
	 * @since 1.1.4
	 * @version 1.1.4
	 */
	public function account(WP_REST_Request $request){
		$token_valid = $this->validate_token( $request );
		if($token_valid !== true){
			return $token_valid;
		}

		$account_id = $this->gateway_instance->get_option( 'stripe_connect_id' );
		if(empty( $account_id )){
			return new WP_Error( 'empty_id', __( 'The WP Admin has not provided a Stripe Connect account id.', 'terminal-for-stripe-and-woocommerce' ), array( 'status' => 500 ) );
		}

		return rest_ensure_response( $account_id );
	}

	/**
	 * Returns the api app version.
	 *
	 * @since 1.1.4
	 * @version 1.1.4
	 */
	public function version(){
		return rest_ensure_response( self::$APP_VERSION );
	}

	/**
	 * Returns a paginated resultset of all published products.
	 *
	 * @since 1.1.4
	 * @version 1.1.4
	 */
	public function products(WP_REST_Request $request){
		$token_valid = $this->validate_token( $request );
		if($token_valid !== true){
			return $token_valid;
		}

		$product_counts = new WP_Query( array(
			'post_type' => 'product',
			'post_status' => 'publish',
			'posts_per_page' => -1
		) );
		if($product_counts->found_posts <= 0){
			return new WP_Error( 'no_published_products', __( 'The WP Admin has not published any products.', 'terminal-for-stripe-and-woocommerce' ), array( 'status' => 500 ) );
		}

		$page = $request->has_param( 'page' ) ? intval( $request->get_param( 'page' ) ) : 1;
		$products = apply_filters( 'wc_products_apply_get_data', wc_get_products( array(
			'limit' => 20,
			'page' => $page,
			'paginate' => true
		) ) );

		return new WP_REST_Response( $products );
	}

	/**
	 * Returns the product associated with the provided id.
	 *
	 * @since 1.1.4
	 * @version 1.1.4
	 */
	public function product(WP_REST_Request $request){
		$token_valid = $this->validate_token( $request );
		if($token_valid !== true){
			return $token_valid;
		}

		$product = wc_get_product( intval( $request->get_param( 'id' ) ) );
		if(empty( $product )){
			return new WP_Error( 'product_not_found', __( 'We could not find a product with the given product id.', 'terminal-for-stripe-and-woocommerce' ), array( 'status' => 404 ) );
		}

		$data = $product->get_data();
		$data['has_variants'] = method_exists( $product, 'get_available_variations' );
		return new WP_REST_Response( $data );
	}

	/**
	 * Returns the product variants associated with the provided product id.
	 *
	 * @since 1.1.4
	 * @version 1.1.4
	 */
	public function product_variants(WP_REST_Request $request){
		$token_valid = $this->validate_token( $request );
		if($token_valid !== true){
			return $token_valid;
		}

		$product = wc_get_product( intval( $request->get_param( 'id' ) ) );
		if(empty( $product )){
			return new WP_Error( 'product_not_found', __( 'We could not find a product with the given product id.', 'terminal-for-stripe-and-woocommerce' ), array( 'status' => 404 ) );
		}

		if(!method_exists( $product, 'get_available_variations' )){
			return new WP_Error( 'invalid_product', __( 'The provided product is not a variable product.', 'terminal-for-stripe-and-woocommerce' ), array( 'status' => 400 ) );
		}
		
		return new WP_REST_Response( $product->get_available_variations() );
	}

	/**
	 * Creates a new order.
	 *
	 * @since 1.1.4
	 * @version 1.1.4
	 */
	public function create_order(WP_REST_Request $request){
		$token_valid = $this->validate_token( $request );
		if($token_valid !== true){
			return $token_valid;
		}

		$data = $request->get_json_params();
		if(!empty( $data )){
			$data['payment_method'] = $this->gateway_instance->id;
			$data['payment_method_title'] = $this->gateway_instance->get_title();
			$data['set_paid'] = false;
			unset( $data['customer_id'] );
			if(!empty( $data['line_items'] )){
				foreach($data['line_items'] as $key => $line_item){
					if(!empty( $line_item['variation_id'] )){
						$product = wc_get_product( intval( $line_item['product_id'] ) );
						if(method_exists( $product, 'get_available_variations' )){
							$found = false;
							$variation_id = intval( $line_item['variation_id'] );
							$variations = $product->get_available_variations();
							foreach($variations as $variation){
								if($variation_id === $variation['variation_id']){
									$found = true;
									break;
								}
							}
							
							if($found === false){
								$message = sprintf( __( 'The provided variation "%d" is not a valid variation of the product "%d"', 'terminal-for-stripe-and-woocommerce' ), $variation_id, $line_item['product_id'] );
								return new WP_Error( 'invalid_variation', $message, array( 'status' => 400 ) );
							}

							$data['line_items'][$key]['product_id'] = $variation_id;
						}

						unset( $data['line_items'][$key]['variation_id'] );
					}
				}
			}
			$data = array( 'order' => $data );
		}

		$order_api = new WC_API_Orders( new WC_API_Server( '/' ) );
		$response = $order_api->create_order( $data );
		if($response instanceof WP_Error || empty($response) || empty($response['order'])){
			return $response;
		}

		$order = wc_get_order( $response['order']['id'] );
		$order->update_meta_data( '_admin_user_id', wp_get_current_user()->ID );
		$order->save();
		return $order_api->get_order( $order->get_id() );
	}

	/**
	 * Get an existing order
	 *
	 * @since 1.1.4
	 * @version 1.1.4
	 */
	public function get_order(WP_REST_Request $request){
		$token_valid = $this->validate_token( $request );
		if($token_valid !== true){
			return $token_valid;
		}

		$order = wc_get_order( intval( $request->get_param( 'id' ) ) );
		if(empty( $order )){
			return new WP_Error( 'order_not_found', __( 'We could not find an order with the given order id.', 'terminal-for-stripe-and-woocommerce' ), array( 'status' => 404 ) );
		}

		$order_api = new WC_API_Orders( new WC_API_Server( '/' ) );
		return $order_api->get_order( $order->get_id() );
	}

	/**
	 * Update an existing order
	 *
	 * @since 1.1.4
	 * @version 1.1.4
	 */
	public function update_order(WP_REST_Request $request){
		$token_valid = $this->validate_token( $request );
		if($token_valid !== true){
			return $token_valid;
		}

		$order = wc_get_order( intval( $request->get_param( 'id' ) ) );
		if(empty( $order )){
			return new WP_Error( 'order_not_found', __( 'We could not find an order with the given order id.', 'terminal-for-stripe-and-woocommerce' ), array( 'status' => 404 ) );
		}

		$data = $request->get_json_params();
		if(!empty( $data )){
			if(isset( $data['customer_id'] )){
				unset( $data['customer_id'] );
			}

			if(!empty( $data['line_items'] )){
				foreach($data['line_items'] as $key => $line_item){
					if(!empty( $line_item['variation_id'] )){
						$product = wc_get_product( intval( $line_item['product_id'] ) );
						if(method_exists( $product, 'get_available_variations' )){
							$found = false;
							$variation_id = intval( $line_item['variation_id'] );
							$variations = $product->get_available_variations();
							foreach($variations as $variation){
								if($variation_id === $variation['variation_id']){
									$found = true;
									break;
								}
							}
							
							if($found === false){
								$message = sprintf( __( 'The provided variation "%d" is not a valid variation of the product "%d"', 'terminal-for-stripe-and-woocommerce' ), $variation_id, $line_item['product_id'] );
								return new WP_Error( 'invalid_variation', $message, array( 'status' => 400 ) );
							}

							$data['line_items'][$key]['product_id'] = $variation_id;
						}

						unset( $data['line_items'][$key]['variation_id'] );
					}
				}
			}else{
				$data['line_items'] = array();
			}

			$data = array( 'order' => $data );
		}

		$order_api = new WC_API_Orders( new WC_API_Server( '/' ) );
		$response = $order_api->edit_order( $order->get_id(), $data );
		if($response instanceof WP_Error || empty($response) || empty($response['order'])){
			return $response;
		}

		$order = wc_get_order( $response['order']['id'] );
		$order->save();
		return $order_api->get_order( $order->get_id() );
	}

	/**
	 * Deletes an existing order
	 *
	 * @since 1.1.4
	 * @version 1.1.4
	 */
	public function delete_order(WP_REST_Request $request){
		$token_valid = $this->validate_token( $request );
		if($token_valid !== true){
			return $token_valid;
		}

		$order = wc_get_order( intval( $request->get_param( 'id' ) ) );
		if(empty( $order )){
			return new WP_Error( 'order_not_found', __( 'We could not find an order with the given order id.', 'terminal-for-stripe-and-woocommerce' ), array( 'status' => 404 ) );
		}

		$order->delete( true );
		return rest_ensure_response( 'deleted' );
	}

	/**
	 * Gets all available shipping methods
	 *
	 * @since 1.1.4
	 * @version 1.1.4
	 */
	public function shipping_methods(WP_REST_Request $request){
		$token_valid = $this->validate_token( $request );
		if($token_valid !== true){
			return $token_valid;
		}

		$order = wc_get_order( intval( $request->get_param( 'id' ) ) );
		if(empty( $order )){
			return new WP_Error( 'order_not_found', __( 'We could not find an order with the given order id.', 'terminal-for-stripe-and-woocommerce' ), array( 'status' => 404 ) );
		}

		$order_data = $order->get_data();
		if(empty( $order_data ) || empty( $order_data['shipping'] ) || empty( $order_data['shipping']['address_1'] )){
			return new WP_Error( 'no_shipping_address', __( 'You must set a shipping address to the order before retrieving shipping methods.', 'terminal-for-stripe-and-woocommerce' ), array( 'status' => 400 ) );
		}

		if( WC()->cart ){
			WC()->cart->empty_cart();
		}

		$shipping = $order_data['shipping'];
		$line_items = $order->get_items( 'line_item' );
		
		if ( defined( 'WC_ABSPATH' ) ) {
			// WC 3.6+ - Cart and other frontend functions are not included for REST requests.
			include_once WC_ABSPATH . 'includes/wc-cart-functions.php';
			include_once WC_ABSPATH . 'includes/wc-notice-functions.php';
			include_once WC_ABSPATH . 'includes/wc-template-hooks.php';
		}

		if ( null === WC()->session ) {
			$session_class = apply_filters( 'woocommerce_session_handler', 'WC_Session_Handler' );
			WC()->session = new $session_class();
			WC()->session->init();
		}

		WC()->cart = new SFTW_Cart($shipping);
		foreach($line_items as $item){
			$product = $item->get_product();
			$qty = !empty($item->get_quantity())? $item->get_quantity() : 1;
			$variant = !empty($item->get_variation_id())? $item->get_variation_id() : null;
			$id = $product->id;
			WC()->cart->add_to_cart($id, $qty, $variant);
		}

		$shipping = new WC_Shipping();
		$package = $shipping->calculate_shipping_for_package( WC()->cart->get_shipping_packages()[0] );
		$available_methods = array();
		if(!empty($package['rates'])){
			foreach($package['rates'] as $rate){
				$available_methods[] = array(
					'method_id' => $rate->get_id(),
					'method_title' => $rate->get_label(),
					'total' => $rate->get_cost()
				);
			}
		}

		// Cleanup
		WC()->cart = null;
		wc_empty_cart();
		return new WP_REST_Response( $available_methods );
	}
}

class SFTW_Cart extends WC_Cart{
	private $customer;

	public function __construct($shipping){
		parent::__construct();
		$this->customer = new WC_Customer();
		if(!empty($shipping['first_name'])){
			$this->customer->set_shipping_first_name($shipping['first_name']);
		}
		if(!empty($shipping['last_name'])){
			$this->customer->set_shipping_last_name($shipping['last_name']);
		}
		if(!empty($shipping['company'])){
			$this->customer->set_shipping_company($shipping['company']);
		}
		if(!empty($shipping['address_1'])){
			$this->customer->set_shipping_address_1($shipping['address_1']);
		}
		if(!empty($shipping['address_2'])){
			$this->customer->set_shipping_address_2($shipping['address_2']);
		}
		if(!empty($shipping['city'])){
			$this->customer->set_shipping_city($shipping['city']);
		}
		if(!empty($shipping['state'])){
			$this->customer->set_shipping_state($shipping['state']);
		}
		if(!empty($shipping['postcode'])){
			$this->customer->set_shipping_postcode($shipping['postcode']);
		}
		if(!empty($shipping['country'])){
			$this->customer->set_shipping_country($shipping['country']);
		}
	}

	public function get_customer() {
		return $this->customer;
	}
}